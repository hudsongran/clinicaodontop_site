<?php

include "aq_resize.php";

function debug($value){
    echo '<pre>';
    print_r($value);
    echo '</pre>';
}


function generateThumb($options = array()){
    $default = array('src'=>'','h'=>100,'w'=>100,'crop'=>true);
    $options = array_merge($default,$options);

    $image = aq_resize($options['src'], $options['w'], $options['h'] , $options['crop'] );

    return $image;
}


function generateAttachmentFirst($postID = 1){
    if($postID){
        $args = array(
            'post_type' => 'attachment',
            'numberposts' => -1,
            'post_status' => null,
            'post_parent' => get_the_ID()
        );
        $attachments = get_posts( $args );

    } else {
        $args = array(
            'post_type' => 'attachment',
            'numberposts' => -1,
            'post_status' => null,
            'post_parent' => $postID
        );
        $attachments = get_posts( $args );
    }
    if($attachments[0]->guid){
        return $attachments[0]->guid;
    } else {
        return false;
    }
}

function getNowURL() {
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    }
    else
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function truncate($text, $max, $options = array()) {
    $options = array_merge(array('end'=>'...', 'exact' => true), $options);
    if (strlen($text) > $max) {
        $text = substr($text, 0, $max);
        if (!$options['exact']) {
            $text = substr($text,0,strrpos($text," "));
        }
        $text .= $options['end'];
    }
    return $text;
}

function custom_taxonomies_terms_links() {
	global $post, $post_id;
	// get post by post id
	$post = &get_post($post->ID);
	// get post type by post
	$post_type = $post->post_type;
	// get post type taxonomies
	$taxonomies = get_object_taxonomies($post_type);
	foreach ($taxonomies as $taxonomy) {
		// get the terms related to post
		$terms = get_the_terms( $post->ID, $taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $term )
				$out[] = '<a href="' .get_term_link($term->slug, $taxonomy) .'">'.$term->name.'</a>';
			$return = join( ', ', $out );
		}
		return $return;
	}
}

function pagination( $query, $baseURL = '' ) {
    if ( ! $baseURL ) $baseURL = get_bloginfo( 'url' );
    $page = $query->query_vars["paged"];
    if ( !$page ) $page = 1;
    $qs = $_SERVER["QUERY_STRING"] ? "?".$_SERVER["QUERY_STRING"] : "";
    // Only necessary if there's more posts than posts-per-page
    if ( $query->found_posts > $query->query_vars["posts_per_page"] ) {
        echo '<ul class="paging">';
        // Previous link?
        if ( $page > 1 ) {
            echo '<li class="previous"><a href="'.$baseURL.'page/'.($page-1).'/'.$qs.'">« previous</a></li>';
        }
        // Loop through pages
        for ( $i=1; $i <= $query->max_num_pages; $i++ ) {
            // Current page or linked page?
            if ( $i == $page ) {
                echo '<li class="active">'.$i.'</li>';
            } else {
                echo '<li><a href="'.$baseURL.'page/'.$i.'/'.$qs.'">'.$i.'</a></li>';
            }
        }
        // Next link?
        if ( $page < $query->max_num_pages ) {
            echo '<li><a href="'.$baseURL.'page/'.($page+1).'/'.$qs.'">next »</a></li>';
        }
        echo '</ul>';
    }
}
    function CompressURL($url) {

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, "http://to.ly/api.php?longurl=".urlencode($url));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_HEADER, 0);

      $shorturl = curl_exec ($ch);
      curl_close ($ch);

      return $shorturl;
    }
