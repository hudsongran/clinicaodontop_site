<?php


/**
 * Remove itens do menu
 */


function remove_screen_options(){
    return false;
}
add_filter('screen_options_show_screen', 'remove_screen_options');

/**
 * Remove itens do admin_bar
 */
function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('updates');
    $wp_admin_bar->remove_menu('new-content');

}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

function remove_admin_menu()
{

  // remove_menu_page('index.php'); // Dashboard
  // remove_menu_page('update-core.php'); // Dashboard
  // remove_menu_page('edit.php'); // Posts
  // remove_menu_page('link-manager.php'); // Links
  // remove_menu_page('edit-comments.php'); // Comments
  // remove_menu_page('themes.php'); // Appearance
  // remove_menu_page('edit.php?post_type=page'); // Pages
  // remove_menu_page('upload.php'); // Media
  // remove_menu_page('plugins.php'); // Plugins
  // remove_menu_page('tools.php'); // Tools
  // remove_menu_page('options-general.php'); // Settings
  // remove_menu_page('edit.php?post_type=acf'); // ACF
  // remove_menu_page('admin.php?page=blue_admin'); // ACF
 
}
 
add_action( 'admin_menu', 'remove_admin_menu', 999 );


if(function_exists("register_options_page"))
{
    register_options_page('Sobre Meramente Mae');
    register_options_page('Redes Sociais');
}


function single_screen_columns( $columns ) {
    $columns['dashboard'] = 1;
    return $columns;
}
add_filter( 'screen_layout_columns', 'single_screen_columns' );

function single_screen_dashboard(){return 1;}
add_filter( 'get_user_option_screen_layout_dashboard', 'single_screen_dashboard' );

function my_custom_login_logo() {
    echo '<style type="text/css">
        #login {
          width:369px;
          background: url('.get_template_directory_uri().'/logo.png) no-repeat !important;
          background-position: 0px 115px !important
           
        }
        input#wp-submit {
          background: #e74c3c !important;
          background-image: -webkit-gradient(linear,left top,left bottom,from(#e74c3c),to(#d80000)) !important;
          background-image: -webkit-linear-gradient(top,#e74c3c,#d80000) !important;
          background-image: -moz-linear-gradient(top,#e74c3c,#d80000) !important;
          background-image: -ms-linear-gradient(top,#e74c3c,#d80000) !important;
          background-image: -o-linear-gradient(top,#e74c3c,#d80000) !important;
          background-image: linear-gradient(to bottom,#e74c3c,#d80000) !important;
          border-color: #d80000 !important;
          border-bottom-color: #d80000 !important;
          -webkit-box-shadow: inset 0 1px 0 #d80000 !important;
          box-shadow: inset 0 1px 0 #d80000 !important;
          color: #fff !important;
          text-decoration: none !important;
          text-shadow: 0 1px 0 rgba(0,0,0,0.1) !important;
        }
        #loginform {
           
        }
        h1 a { background: none !important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

add_action('admin_head', 'my_custom_logo');

function my_custom_logo() {
    echo '<style type="text/css">
         #header-logo { background-image: url('.get_template_directory_uri().'/logo.png) !important; }</style>';
}


function custom_colors() {
    echo '<style type="text/css">
      body.index-php #wpbody{
        min-height: 500px;
        background:url('.get_template_directory_uri().'/logo.png) no-repeat center !important;
        background-size: 369px !important;
      }
      #dashboard-widgets-wrap{display:none;}
      </style>';
}

add_action('admin_head', 'custom_colors');

//---------------------------------------------------------------------------------//


/**
 * Remover botão de help
 */
function hide_help() {
    echo '<style type="text/css">
            #contextual-help-link-wrap { display: none !important; }
          </style>';
}
add_action('admin_head', 'hide_help');

//---------------------------------------------------------------------------------//

/**
 * Remove submenu
 */
function remove_submenus() {
  global $submenu;
  unset($submenu['index.php'][10]); // Removes 'Updates'.
  // unset($submenu['themes.php'][5]); // Removes 'Themes'.
  unset($submenu['options-general.php'][15]); // Removes 'Writing'.
  unset($submenu['options-general.php'][25]); // Removes 'Discussion'.
  unset($submenu['edit.php'][16]); // Removes 'Tags'.
}
add_action('admin_menu', 'remove_submenus');




/**
 * Remove widgets do dashboard
 */
function remove_dashboard_widgets(){
    global$wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['icl_dashboard_widget']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');


//---------------------------------------------------------------------------------//


/**
 * Substitui a mensagem esquerda do footer
 */
function remove_footer_admin () {
    echo '&copy ' . date('Y');
}
add_filter('admin_footer_text', 'remove_footer_admin');



//---------------------------------------------------------------------------------//



/**
 * Substitui a versão no footer
 */
function change_footer_version() {
    return '';
}
add_filter( 'update_footer', 'change_footer_version', 9999 );


function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
// add_filter('pre_site_transient_update_core','remove_core_updates');
// add_filter('pre_site_transient_update_plugins','remove_core_updates');
// add_filter('pre_site_transient_update_themes','remove_core_updates');