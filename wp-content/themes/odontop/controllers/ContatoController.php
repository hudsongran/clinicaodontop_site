<?php 
ob_start();
include '../plugins/phpmailer/class.phpmailer.php';

class ContactForm {

	private $paramsForm = array();
	private $error = array();

	public function setData($pForm = array ()) {

		if(!empty($pForm)){
			$this->paramsForm = $pForm;
		}

		return $this;
	}

	public function validateData() {

		$campos['nome'] = $this->paramsForm['name'];
		$campos['sobrenome'] = $this->paramsForm['lastname'];
		$campos['email'] = $this->paramsForm['email'];
		$campos['telefone'] = $this->paramsForm['phone'];
		$campos['sede'] = $this->paramsForm['sede'];
		$campos['assunto'] = $this->paramsForm['message'];

		$error = 0;
		$campoEmBranco = '';
		foreach($campos as $key => $value){
			if(empty($value)){
				$campoEmBranco .= ucfirst($key) . ', ';
				$error = 1;
			}
		}

		if($error){
			$this->error['erro'] = 'Os seguintes campos são obrigatórios: ' . $campoEmBranco;
			$this->error['erro'] = rtrim($this->error['erro'], ", ");
		}

		return $this;

	}

	public function sendMail() {
		
		if(count($this->error) > 0) {

			$dataReturn['status'] = false;
			$dataReturn['validate'] = $this->error;

		} else {

			$phpMailer = new PHPMailer();

			$phpMailer->IsHTML(true);
			$phpMailer->CharSet = 'UTF-8';
			$phpMailer->From = 'contato@clinicaodontop.com.br';
			$phpMailer->FromName = '[Odontop]';
			
			$phpMailer->AddAddress('willerson@handgran.com');

			$phpMailer->Subject = "Contato";

			$phpMailer->Body .= "<strong>Nome:</strong> ".$this->paramsForm['name']."<br>";
			$phpMailer->Body .= "<strong>Sobrenome:</strong> ".$this->paramsForm['lastname']."<br>";
			$phpMailer->Body .= "<strong>E-mail: </strong>".$this->paramsForm['email']."<br>";
			$phpMailer->Body .= "<strong>Telefone: </strong>".$this->paramsForm['phone']."<br>";

			$phpMailer->Body .= "<strong>Sede:</strong> ".$this->paramsForm['sede']."<br>";
			
			$phpMailer->Body .= "<strong>Mensagem:</strong> <br>".nl2br($this->paramsForm['message'])."";
		

			$sender = $phpMailer->Send();

			if(!$sender){
				$dataReturn['status'] = false;
				$this->error['email_send'] = 'Erro ao enviar e-mail!';
				$dataReturn['validate'] = $this->error;

			} else {
				$dataReturn['status'] = true;
				$dataReturn['validate'] = array();
			}
			
		}

		return json_encode($dataReturn);
	}
}



$contactForm = new ContactForm();
echo $contactForm->setData($_POST['form'])
			->validateData()
			->sendMail();


?>