<?php get_header(); ?>

    <div class="container">
        <div class="mapa-navegacao">
            <h3><a href="<?php echo HOME; ?>">Home</a> / <strong>Tratamentos</strong></h3>
        </div>
    </div>

    <section class="tratamento">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 tira-padding">
                        <div class="menu-tratamentos">
                            <ul>
                                <?php $services = getServicessName(); ?>
                                <?php $countServices = 0; ?>
                                <?php foreach ($services as $service): ?>
                                <li><a href="<?php echo $service['permalink']; ?>"><button class="<?php echo ($countServices == 0) ? 'active' : ''; ?>"><?php echo $service['name']; ?></button></a></li>
                                <?php $countServices++; ?>
                                <?php endforeach; ?>
                            </ul>   
                        </div>
                    </div>
                </div>

                <?php
                    $argsServicos = array(
                        'post_type'      => 'tratamentos',
                        'posts_per_page' => '1',
                        'orderby'        => 'desc'
                    );

                    $loopServicos = new WP_Query( $argsServicos );
                    if($loopServicos->have_posts() ) :
                        while ( $loopServicos->have_posts() ) : $loopServicos->the_post();
                ?>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  list-tratamento">
                    <div class="box-tratamento">
                        <img src="<?php echo get_field('imagem'); ?>" alt="">
                        <h3><?php the_title(); ?></h3>

                        <?php echo get_field('descricao'); ?>
                    </div>
                    <div class="carousel-tratamento">
                        <ul class="slider-tratamentos">
                            <?php $galeria = get_field('galeria'); ?>
                            <?php foreach ($galeria as $image): ?>
                            <li><a href="<?php echo $image['url']; ?>" data-lightbox="roadtrip" data-title="<?php echo get_the_title(); ?>"><img src="<?php echo $image['url']; ?>" /></a></li>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                </div>
                <?php
                    endwhile;
                        wp_reset_query();
                    endif;
                ?>


            </div>
        </div>
    </section>

<?php get_footer(); ?>