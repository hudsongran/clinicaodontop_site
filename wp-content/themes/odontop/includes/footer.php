	<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <h4>Nossos Serviços</h4>
                    <ul>
                        <li><a href="#">Ortodontia</a></li>
                        <li><a href="#">Implantodontia</a></li>
                        <li><a href="#">Estética</a></li>
                        <li><a href="#">Clareamento Dental</a></li>
                        <li><a href="#">Prótese</a></li>
                        <li><a href="#">Periodontia</a></li>
                        <li><a href="#">DTM</a></li>
                        <li><a href="#">Ronco e Apneia</a></li>
                        <li><a href="#">Odontopediatria</a></li>
                        <li><a href="#">Endodotia</a></li>
                        <li><a href="#">Extração de Siso</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <h4>Nossas Sedes</h4>
                    <ul>
                        <li><a href="#">Alm. Tamandaré</a></li>
                        <li><a href="#">Rio Branco</a></li>
                        <li><a href="#">Araucária</a></li>
                        <li><a href="#">Coritiba</a></li>
                        <li><a href="#">Colombo</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <h4>Telefones e Horários</h4>
                    <select>
                        <option value="Colombo">Colombo</option>
                    </select>
                    <h2><img src="images/icon-fone-footer.png" alt="">41 3224-2989</h2>
                    <h2><img src="images/icon-whats-footer.png" alt="">41 99876-2234</h2>
                    <p>Segunda à Sexta</p>
                    <span>8h às 18h</span>
                    <p>Sábado</p>
                    <span>8h às 14h</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <h4>Redes Sociais</h4>
                    <ul>
                        <li><a href="#"><img src="images/icon-facebook-footer.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>   
    </footer>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tira-padding">
                <div class="agendar-footer">
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-12 col-sm-12">
                            <img src="images/icon-calendar-o.png" alt="">
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <h3>Agende sua consulta sem compromisso.</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <button>Agendar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="rodape">
        <div class="container">
            <div class="row">
                <div class="cophy">
                    <p>© Odontop - Todos os direitos reservados 2017</p>
                </div>
                <div class="dev">
                    <p>Desenvolvido por: <img src="images/icon-dev.png" alt=""></p>
                </div>
            </div>
        </div>
    </div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type"text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type"text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type"text/javascript" src="bower_components/bxSlider/dist/jquery.bxslider.js"></script>
    <script type"text/javascript" src="js/lightbox.js"></script>
    <script type"text/javascript" src="js/jquery.nice-select.min.js"></script>	
    <script type"text/javascript" src="js/main.js"></script>	
    
  </body>
</html>