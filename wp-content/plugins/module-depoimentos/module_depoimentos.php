<?php

/*
  Plugin Name: Módulo de Depoimentos
  Description: Módulo de Depoimentos
  Version: 1.0
  Author: Victor Magalhães
  Author URI: http://magalhaesvictor.com.br
 */

add_action("init", "moduleDepoimentos");

function moduleDepoimentos() {
    $init_class = new ModuleDepoimentos();
    $init_class->init();
}

class ModuleDepoimentos {

    function ModuleDepoimentos() {
        
    }

	

    function init() {
	
		$labels = array(
			'name' => 'Depoimentos',
			'singular_name' => 'Depoimento',
			'add_new' => 'Adicionar novo',
			'add_new_item' => 'Adicionar novo depoimento',
			'edit_item' => 'Editar Depoimento',
			'new_item' => 'Novo Depoimento',
			'all_items' => 'Todos os Depoimentos',
			'view_item' => 'Ver Depoimento',
			'search_items' => 'Pesquisar Depoimento',
			'not_found' =>  'Nenhum depoimento encontrado',
			'not_found_in_trash' => 'Nenhum depoimento encontrado no lixo',
			'parent_item_colon' => 'Depoimento pai',
			'menu_name' => 'Depoimentos'
		);
		$args = array(
			'labels' => $labels,
			'description' => '',
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'menu_icon' => WP_PLUGIN_URL.'/module-depoimentos/icon.png',
			'hierarchical' => false,
			'supports' => array(
				'title'
			),
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'depoimento',
				'with_front' => false,
				'feeds' => true,
				'pages' => true,
			),
			'query_var' => 'depoimentos',
			'can_export' => true,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',
		);
		
        register_post_type('depoimentos',$args);
		
		// Da um flush na geração de regras de navegação
        flush_rewrite_rules();

    }

}

?>