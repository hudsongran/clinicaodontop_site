<?php

/*
  Plugin Name: Módulo de Sedes
  Description: Módulo de Sedes
  Version: 1.0
  Author: Victor Magalhães
  Author URI: http://magalhaesvictor.com.br
 */

add_action("init", "moduleSedes");

function moduleSedes() {
    $init_class = new ModuleSedes();
    $init_class->init();
}

class ModuleSedes {

    function ModuleSedes() {
        
    }

	

    function init() {
	
		$labels = array(
			'name' => 'Sedes',
			'singular_name' => 'Sede',
			'add_new' => 'Adicionar novo',
			'add_new_item' => 'Adicionar nova Sede',
			'edit_item' => 'Editar Sede',
			'new_item' => 'Nova Sede',
			'all_items' => 'Todas as Sedes',
			'view_item' => 'Ver Sede',
			'search_items' => 'Pesquisar Sede',
			'not_found' =>  'Nenhuma Sede encontrada',
			'not_found_in_trash' => 'Nenhuma Sede encontrada no lixo',
			'parent_item_colon' => 'Sede pai',
			'menu_name' => 'Sedes'
		);
		$args = array(
			'labels' => $labels,
			'description' => '',
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'menu_icon' => WP_PLUGIN_URL.'/module-sedes/icon.png',
			'hierarchical' => false,
			'supports' => array(
				'title'
			),
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'sedes',
				'with_front' => false,
				'feeds' => true,
				'pages' => true,
			),
			'query_var' => 'sedes',
			'can_export' => true,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',
		);
		
        register_post_type('sedes',$args);
		
		// Da um flush na geração de regras de navegação
        flush_rewrite_rules();

    }

}

?>