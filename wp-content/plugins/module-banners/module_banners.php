<?php

/*
  Plugin Name: Módulo de Banners
  Description: Módulo de Banners
  Version: 1.0
  Author: Victor Magalhaes
  Author URI: http://magalhaesvictor.com.br
 */

add_action("init", "moduleBanners");

function moduleBanners() {
    $init_class = new ModuleBanners();
    $init_class->init();
}

class ModuleBanners {

    function moduleBanners() {
        
    }

	

    function init() {
	
		$labels = array(
			'name' => 'Banners',
			'singular_name' => 'Banner',
			'add_new' => 'Adicionar novo',
			'add_new_item' => 'Adicionar novo Banner',
			'edit_item' => 'Editar Banner',
			'new_item' => 'Novo Banner',
			'all_items' => 'Todos os Banners',
			'view_item' => 'Ver Banner',
			'search_items' => 'Pesquisar Banner',
			'not_found' =>  'Nenhum Banner encontrado',
			'not_found_in_trash' => 'Nenhum Banner encontrado no lixo',
			'parent_item_colon' => 'Banner pai',
			'menu_name' => 'Banners'
		);
		$args = array(
			'labels' => $labels,
			'description' => '',
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'menu_icon' => WP_PLUGIN_URL.'/module-banners/icon.png',
			'hierarchical' => false,
			'supports' => array(
				'title'
			),
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'banners',
				'with_front' => false,
				'feeds' => true,
				'pages' => true,
			),
			'query_var' => 'banners',
			'can_export' => true,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',
		);
		
        register_post_type('banners',$args);
		
		// Da um flush na geração de regras de navegação
        flush_rewrite_rules();

    }

}

?>