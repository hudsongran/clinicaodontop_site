<?php

/*
  Plugin Name: Módulo de Doutores
  Description: Módulo de Doutores
  Version: 1.0
  Author: Victor Magalhães
  Author URI: http://magalhaesvictor.com.br
 */

add_action("init", "moduleDoutores");

function moduleDoutores() {
    $init_class = new ModuleDoutores();
    $init_class->init();
}

class ModuleDoutores {

    function ModuleDoutores() {
        
    }

	

    function init() {
	
		$labels = array(
			'name' => 'Doutores',
			'singular_name' => 'Doutor',
			'add_new' => 'Adicionar novo',
			'add_new_item' => 'Adicionar novo doutor',
			'edit_item' => 'Editar Doutor',
			'new_item' => 'Novo Doutor',
			'all_items' => 'Todos os Doutores',
			'view_item' => 'Ver Doutor',
			'search_items' => 'Pesquisar Doutor',
			'not_found' =>  'Nenhum doutor encontrado',
			'not_found_in_trash' => 'Nenhum doutor encontrado no lixo',
			'parent_item_colon' => 'Doutor pai',
			'menu_name' => 'Doutores'
		);
		$args = array(
			'labels' => $labels,
			'description' => '',
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'menu_icon' => WP_PLUGIN_URL.'/module-doutores/icon.png',
			'hierarchical' => false,
			'supports' => array(
				'title'
			),
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'doutor',
				'with_front' => false,
				'feeds' => true,
				'pages' => true,
			),
			'query_var' => 'doutores',
			'can_export' => true,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',
		);
		
        register_post_type('doutores',$args);
		
		// Da um flush na geração de regras de navegação
        flush_rewrite_rules();

    }

}

?>